#!/bin/bash

wait-for-it ${DB_HOST}:${DB_PORT} -- supervisord -c /etc/supervisor/conf.d/supervisord.conf
