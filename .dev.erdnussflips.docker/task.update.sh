#/bin/bash

TTRSS_VERSION=${TTRSS_VERSION:=master}



git fetch
git checkout $TTRSS_VERSION

chown www-data:www-data -R /var/www \
	&& cp config.php-dist config.phpchown www-data:www-data -R /var/www \
	&& cp config.php-dist config.php

git clone --progress --verbose https://git.tt-rss.org/fox/tt-rss.git . && git checkout $TTRSS_VERSION \
&& chown www-data:www-data -R /var/www \
&& cp config.php-dist config.php