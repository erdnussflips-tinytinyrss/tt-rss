# --------------------------------------------------------
# ------------------------ TT-RSS base -------------------
# --------------------------------------------------------
FROM php:7.3-fpm as base

LABEL maintainer="erdnussflips"

# install some tools
RUN \
	apt-get update \
	&& apt-get install --no-install-recommends --no-install-suggests --yes \
		ca-certificates \
		supervisor \
		curl \
		git \
		nginx \
		wait-for-it \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*

# RUN mkdir /run/php && touch /run/php/php7.0-fpm.sock
# create php hardlink (PHP_EXECUTABLE)
RUN ln /usr/local/bin/php /usr/bin/php

# install php plugins
# see https://github.com/docker-library/docs/tree/master/php#how-to-install-more-php-extensions
RUN \
	# check for pre installed plugins
	php -m | grep --ignore-case --line-regexp "curl" \
	&& php -m | grep --ignore-case --line-regexp "mbstring" \
	&& php -m | grep --ignore-case --line-regexp "json" \
	&& php -m | grep --ignore-case --line-regexp "xml" \
	&& php -m | grep --ignore-case --line-regexp "dom" \
	&& php -m | grep --ignore-case --line-regexp "iconv" \
	&& php -m | grep --ignore-case --line-regexp "mysqlnd" \
	&& php -m | grep --ignore-case --line-regexp "posix" \
	&& php -m | grep --ignore-case --line-regexp "pdo" \
	&& BUILD_DEPS="" && RUNTIME_DEPS="" \
		# intl deps
		&& BUILD_DEPS="${BUILD_DEPS} libicu-dev" \
		&& RUNTIME_DEPS="${RUNTIME_DEPS} libicu63" \
		# gd deps
		&& BUILD_DEPS="${BUILD_DEPS} libpng-dev" \
		&& RUNTIME_DEPS="${RUNTIME_DEPS} libpng-tools" \
		# pgsql deps
		&& BUILD_DEPS="${BUILD_DEPS} libpq-dev" \
		&& RUNTIME_DEPS="${RUNTIME_DEPS} libpq5" \
		# ldap deps
		&& BUILD_DEPS="${BUILD_DEPS} libldap2-dev" \
		&& RUNTIME_DEPS="${RUNTIME_DEPS} ldap-utils" \
		# mcrypt deps
		&& BUILD_DEPS="${BUILD_DEPS} libmcrypt-dev" \
		&& RUNTIME_DEPS="${RUNTIME_DEPS} mcrypt" \
		# zip deps (needed for data_migration)
		&& BUILD_DEPS="${BUILD_DEPS} libzip-dev" \
		&& RUNTIME_DEPS="${RUNTIME_DEPS} libzip4" \
	&& apt-get update && apt-get install --no-install-recommends --no-install-suggests --yes $BUILD_DEPS $RUNTIME_DEPS \
	&& docker-php-ext-install --jobs "$(nproc)" \
		intl \
		gd \
		# pgsql extensions
		pdo_pgsql \
			pgsql \
		opcache \
		ldap \
		pcntl \
		mysqli \
		pdo_mysql \
	# pecl plugins
	&& pecl channel-update pecl.php.net \
	## apcu
	&& pecl install apcu-5.1.17 \
	&& docker-php-ext-enable apcu \
	## mcrypt
	&& pecl install mcrypt-1.0.2 \
	&& docker-php-ext-enable mcrypt \
	## zip
	&& pecl install zip-1.18.2 \
	&& docker-php-ext-enable zip \
	# remove build deps
	&& apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false $BUILD_DEPS \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*

# add ttrss as the only nginx site and cleanup www directory
COPY .dev.erdnussflips.docker/ttrss.nginx.conf /etc/nginx/sites-available/ttrss
RUN ln -s /etc/nginx/sites-available/ttrss /etc/nginx/sites-enabled/ttrss \
	&& rm /etc/nginx/sites-enabled/default \
	&& rm -rf /var/www/*

WORKDIR /var/www
COPY --chown=www-data:www-data ./ /var/www/
COPY .dev.erdnussflips.docker/patch-config.php /app/patch-config.php
COPY .dev.erdnussflips.docker/docker-entrypoint.sh /

# Patch configuration
RUN cp --preserve config.php-dist config.php \
	&& chmod -R 777 cache lock feed-icons

ENTRYPOINT [ "/docker-entrypoint.sh" ]

# expose only nginx HTTP port
EXPOSE 80

# expose default configuration via ENV in order to ease overwriting
# default complete path to ttrss
ENV SELF_URL_PATH http://localhost
# default database credentials
ENV DB_NAME ttrss
ENV DB_USER ttrss
ENV DB_PASS ttrss

COPY .dev.erdnussflips.docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY .dev.erdnussflips.docker/docker-command.sh /
CMD [ "/docker-command.sh" ]


# ---------------------------------------------------------
# -------------------- TT-RSS extended --------------------
# ---------------------------------------------------------
FROM base as extended

# install composer
RUN curl -sS https://getcomposer.org/installer | php \
	&& mv composer.phar /usr/local/bin/composer \
	&& chmod +x /usr/local/bin/composer

# Install plugins
RUN cd ./plugins.local \
	# data migration plugin
	&& git clone --progress --verbose --depth 1 https://git.tt-rss.org/fox/ttrss-data-migration.git ./data_migration \
	# fever plugin
	&& git clone --progress --verbose --depth 1 https://github.com/DigitalDJ/tinytinyrss-fever-plugin ./fever \
	# newsplus plugin
	&& git clone --progress --verbose --depth 1 https://github.com/hrk/tt-rss-newsplus-plugin.git ./api_newsplus \
		&& mv -f ./api_newsplus/api_newsplus/* ./api_newsplus/ \
	# feediron plugin
	&& git clone --progress --verbose --depth 1 https://github.com/feediron/ttrss_plugin-feediron.git ./feediron \
		# install Readability.php
		&& cd feediron/filters/fi_mod_readability && composer install && cd - \
	# qrcodegen plugin
	&& git clone --progress --verbose --depth 1 https://github.com/jonrandoem/ttrss-qrcode.git ./qrcodegen \
	# time to read plugin
	&& git clone --progress --verbose --depth 1 https://git.tt-rss.org/fox/ttrss-time-to-read.git ./time_to_read

ARG TTRSS_FEEDLY_THEME_VERSION=master

# Install feedly theme: https://github.com/levito/tt-rss-feedly-theme
RUN cd ./themes.local/ \
	&& git clone --progress --verbose --depth 1 --branch ${TTRSS_FEEDLY_THEME_VERSION} https://github.com/levito/tt-rss-feedly-theme.git ./feedly_theme_download.git \
	&& mv -f ./feedly_theme_download.git/* ./ \
	&& rm -rf ./feedly_theme_download.git


# ---------------------------------------------------------
# -------------------- TT-RSS normal ----------------------
# ---------------------------------------------------------
FROM base as normal
